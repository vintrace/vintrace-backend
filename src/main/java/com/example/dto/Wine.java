package com.example.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.ArrayList;

@Getter @Setter
@AllArgsConstructor
@NoArgsConstructor
public class Wine {
    private String lotCode;
    private double volume;
    private String description;
    private String tankCode;
    private String productState;
    private String ownerName;
    private ArrayList<Component> components;
}
