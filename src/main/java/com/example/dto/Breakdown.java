package com.example.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.ArrayList;

@Setter @Getter
@AllArgsConstructor
@NoArgsConstructor
public class Breakdown {
    private String breakDownType;
    private ArrayList<BreakdownDetail> breakdown;
}
