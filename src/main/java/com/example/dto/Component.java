package com.example.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter @Setter
@AllArgsConstructor
public class Component {
    private double percentage;
    private Long year;
    private String variety;
    private String region;
}
