package com.example.service;

import com.example.dto.BreakdownDetail;
import com.example.dto.Breakdown;
import com.example.dto.Component;
import com.example.dto.Wine;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.springframework.stereotype.Service;

import java.io.FileReader;
import java.io.IOException;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;

@Service
public class WineService{
    private ArrayList<Wine> wines = new ArrayList<>();

    public ArrayList<Wine> getWineList () {
        try {
            Path path = Paths.get("data");
            if(Files.exists(path)) {
                DirectoryStream<Path> ds = Files.newDirectoryStream(path);
                JSONParser jsonParser = new JSONParser();
                for(Path p : ds) {
                    Object obj = jsonParser.parse(new FileReader(String.valueOf(p)));
                    JSONObject jsonObject =  (JSONObject) obj;
                    String lotCode = (String) jsonObject.get("lotCode");
                    Double volume = (Double) jsonObject.get("volume");
                    String description = (String) jsonObject.get("description");
                    String tankCode = (String) jsonObject.get("tankCode");
                    String productState = (String) jsonObject.get("productState");
                    String owner = (String) jsonObject.get("ownerName");
                    ArrayList<JSONObject> componentsGet = (ArrayList) jsonObject.get("components");
                    ArrayList<Component> components = new ArrayList<>();
                    for (JSONObject object: componentsGet) {
                        Double percentage = (Double) object.get("percentage");
                        Long year = (Long) object.get("year");
                        String variety = (String) object.get("variety");
                        String region = (String) object.get("region");
                        Component component = new Component(percentage, year, variety, region);
                        components.add(component);
                    }
                    Wine wine = new Wine(lotCode, volume, description, tankCode, productState, owner, components);
                    addWine(wines, wine);
                }
            }
        }
        catch (IOException err) {
            System.out.println("IO exception error");
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return this.wines;
    }

    public Wine getWineByLotCode(String lotCode) {
        for (Wine wine: wines) {
            if(wine.getLotCode().equals(lotCode)) {
                return wine;
            }
        }
        return new Wine();
    }

    public Breakdown getBreakdown(String lotCode, String breakDownBy) {
        Wine wine = getWineByLotCode(lotCode);
        ArrayList<Component> components = wine.getComponents();
        ArrayList<BreakdownDetail> breakdownDetails = new ArrayList<>();
        if (breakDownBy.equals("year-variety")) {
            for (Component component : components) {
                String percentage = Double.toString(component.getPercentage());
                Long year = component.getYear();
                String variety = component.getVariety();
                String key = year + "-" + variety;
                BreakdownDetail detail = new BreakdownDetail(percentage, key);
                breakdownDetails = addBreakdownDetail(breakdownDetails, detail);
                breakDownBy = "Year & Variety";
            }
        } else {
            for (Component component : components) {
                String percentage = Double.toString(component.getPercentage());
                String key;
                if (breakDownBy.equals("year")) {
                    key = Long.toString(component.getYear());
                } else if (breakDownBy.equals("variety")) {
                    key = component.getVariety();
                } else {
                    key = component.getRegion();
                }
                BreakdownDetail detail = new BreakdownDetail(percentage, key);
                breakdownDetails = addBreakdownDetail(breakdownDetails, detail);
            }
        }

        //Sort
        breakdownDetails.sort((o1, o2) -> {
            double p1 = Double.parseDouble(o1.getPercentage());
            double p2 = Double.parseDouble(o2.getPercentage());
            return (int) (p2 - p1);
        });

        return new Breakdown(breakDownBy, breakdownDetails);
    }

    public ArrayList<BreakdownDetail> addBreakdownDetail (ArrayList<BreakdownDetail> existedList, BreakdownDetail newBreakdownDetail) {
        boolean keyHasInList = false;
        for (BreakdownDetail detail: existedList) {
            if(detail.getKey().equals(newBreakdownDetail.getKey())) {
                String oldPercentage = detail.getPercentage();
                double newPercentage = Double.parseDouble(oldPercentage) + Double.parseDouble(newBreakdownDetail.getPercentage());
                detail.setPercentage(Double.toString(newPercentage));
                keyHasInList = true;
            }
        }

        if(!keyHasInList) {
            existedList.add(newBreakdownDetail);
        }

        return existedList;
    }

    public ArrayList<Wine> addWine (ArrayList<Wine> existedList, Wine newWine) {
        boolean wineHasInList = false;
        for (Wine wine: existedList) {
            if(wine.getLotCode().equals(newWine.getLotCode())) {
                wineHasInList = true;
            }
        }

        if(!wineHasInList) {
            existedList.add(newWine);
        }

        return existedList;
    }
}
