package com.example.controller;

import com.example.dto.Breakdown;
import com.example.service.WineService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/breakdown")
public class BreakdownController {

    @Autowired
    private WineService wineService;

    @GetMapping("/year/{lotCode}")
    public Breakdown breakdownByYear(@PathVariable String lotCode) {
        return wineService.getBreakdown(lotCode, "year");
    }

    @GetMapping("/variety/{lotCode}")
    public Breakdown breakdownByVariety(@PathVariable String lotCode) {
        return wineService.getBreakdown(lotCode, "variety");
    }

    @GetMapping("/region/{lotCode}")
    public Breakdown breakdownByRegion(@PathVariable String lotCode) {
        return wineService.getBreakdown(lotCode, "region");
    }

    @GetMapping("/year-variety/{lotCode}")
    public Breakdown breakdownByYearVariety(@PathVariable String lotCode) {
        return wineService.getBreakdown(lotCode, "year-variety");
    }
}
