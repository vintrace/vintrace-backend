package com.example.controller;

import com.example.dto.Wine;
import com.example.service.WineService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;

@RestController
@RequestMapping("/api")
public class WineController {

    @Autowired
    private WineService wineService;

    @GetMapping("/wineList")
    public ArrayList<Wine> getWineList() {
        return wineService.getWineList();
    }

    @GetMapping("/wine/{lotCode}")
    public Wine getWineDetail(@PathVariable String lotCode) {
        return wineService.getWineByLotCode(lotCode);
    }
}
